# Logo Poll

/poll "`qary` needs a new logo! Which one(s) do you like?" "https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/logo1-small.png" "https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/logo2-small.png" "https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/logo3-small.png" "https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/logo4-small.png" "https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/logo5-small.png"

## Medium

1. https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/logo1.png
2. https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/logo2.png
3. https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/logo3.png
4. https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/logo4.png
5. https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/logo5.png

## All

https://gitlab.com/tangibleai/qary/-/raw/master/docs/logos/