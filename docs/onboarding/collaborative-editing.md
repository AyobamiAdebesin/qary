# Colalborative Editing Webapps

## Django Fanout Collaborative Editor

The editor is pretty basic:

- [Live Example](http://editor.fanoutapp.com/)
- [Fanout Editor Django App](https://github.com/fanout/editor)

It uses one of two proxied PubSub services

- [Fanout Cloud (Pub/Sub service)](https://docs.fanout.io/docs)
- [Open Source Pushpin Server or Paid Service](https://github.com/fanout/pushpin)
