What did you like best about this book?
What did you like least about this book?
What other books did this remind you of?
Which characters in the book did you like best?
Which characters did you like least?
If you were making a movie of this book, who would you cast?
Share a favorite quote from the book. Why did this quote stand out?
What other books by this author have you read? How did they compare to this book?
Would you read another book by this author? Why or why not?
What feelings did this book evoke for you?
What did you think of the book’s length? If it’s too long, what would you cut? If too short, what would you add?
What songs does this book make you think of? Create a book group playlist together!
If you got the chance to ask the author of this book one question, what would it be?
Which character in the book would you most like to meet?
Which places in the book would you most like to visit?
What do you think of the book’s title? How does it relate to the book’s contents? What other title might you choose?
What do you think of the book’s cover? How well does it convey what the book is about? If the book has been published with different covers, which one do you like best?
What do you think the author’s purpose was in writing this book? What ideas was he or she trying to get across?
How original and unique was this book?
If you could hear this same story from another person’s point of view, who would you choose?
What artist would you choose to illustrate this book? What kinds of illustrations would you include?
Did this book seem realistic?
How well do you think the author built the world in the book?
Did the characters seem believable to you? Did they remind you of anyone?
Did the book’s pace seem too fast/too slow/just right?
If you were to write fanfic about this book, what kind of story would you want to tell?
Book Club Questions for Nonfiction
What did you already know about this book’s subject before you read this book?
What new things did you learn?
What questions do you still have?
What else have you read on this topic, and would you recommend these books to others?
What do you think about the author’s research? Was it easy to see where the author got his or her information? Were the sources credible?
Discussion Questions for Memoir
What aspects of the author’s story could you most relate to?
How honest do you think the author was being?
What gaps do you wish the author had filled in? Were there points where you thought he shared too much?
Think about the other people in the book besides the author. How would you feel to have been depicted in this way?
Why do you think the author chose to tell this story?
Book Club Discussion Questions for Short Story and Essay Collections
Which short story/essay did you like best?
Which short story/essay did you like least?
What similarities do these stories share? How do they tie together?
Do you think any of the stories could be expanded into a full-length book?
